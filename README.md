# Jeu video JS

#### Etat du projet
Version 1.0.0
projet en standby

### 1.Description 
Projet réalisé durant ma formation de développeur web chez Simplon.

Nous devions réaliser un jeu afin de réutiliser ce que nous avions vu jusqu'à présent.

Jeu de combat et exploration.

<img src = "screen1.png">
<img src = "screen2.png">



### 2. Fonctionnalitées/techno utilisées
Projet codé en HTML/CSS et JavaScript.

Difficultés en particulier sur la gestion des collisions.



```javascript
function badColl(x, y) {

​    let chara = document.querySelector('.personnage');

​    let buisson = document.querySelectorAll('.collBuiss');

​    for (const item of buisson) {

​        if (x < item.offsetLeft + item.offsetWidth &&

​            x + chara.offsetWidth > item.offsetLeft &&

​            y < item.offsetTop + item.offsetHeight &&

​            chara.offsetHeight + y > item.offsetTop) {

​            return false;

​        } 

​    }return true;

}
```





### 3. Futurs développement
Possibilité de gagner des niveaux pour pouvoir combattre tel ou tel ennemi.


### 4.Licence
CC0


